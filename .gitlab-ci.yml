---
quicktests:
  stage: test
  needs: []
  image: debian:testing
  script:
    - apt-get update
    - apt-get install -y autodep8 libdpkg-perl mypy pycodestyle pyflakes3 python3-debian shellcheck
    - tests/autopkgtest_args
    - tests/mypy
    - tests/pycodestyle
    - tests/pyflakes
    - tests/qemu
    - tests/shellcheck
    - tests/testdesc

.tests: &tests
  script:
    - apt-get update
    - apt-get install -y apt-utils autodep8 build-essential debhelper libdpkg-perl procps python3 python3-debian
    - tests/autopkgtest NullRunner NullRunnerRoot ChrootRunner

tests-testing:
  stage: test
  needs: []
  image: debian:testing
  <<: *tests

tests-stable:
  stage: test
  needs: []
  image: debian:stable
  <<: *tests

tests-bullseye:
  stage: test
  needs: []
  image: debian:bullseye
  <<: *tests

tests-ubuntu-devel:
  stage: test
  needs: []
  image: ubuntu:devel
  allow_failure: true
  <<: *tests

test-docker:
  stage: test
  needs: []
  image: registry.salsa.debian.org/salsa-ci-team/pipeline/autopkgtest
  services:
    - name: docker:dind
      alias: dind
  variables:
    DOCKER_HOST: tcp://dind:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ''
  script:
    - apt-get update
    - |
      apt-get install -y --no-install-recommends \
      adduser \
      apt-utils \
      autodep8 \
      build-essential \
      ca-certificates \
      debhelper \
      debootstrap \
      devscripts \
      docker.io \
      fakeroot \
      libdpkg-perl \
      python3 \
      python3-debian \
      python3-distro-info \
      ${NULL+}
    - AUTOPKGTEST_TEST_UNINSTALLED=yes sh -x debian/tests/docker

test-lxc:
  stage: test
  needs: []
  image: registry.salsa.debian.org/salsa-ci-team/pipeline/autopkgtest
  script:
    - apt-get update
    - |
      apt-get install -y --no-install-recommends \
      apt-utils \
      autodep8 \
      build-essential \
      ca-certificates \
      debhelper \
      debootstrap \
      devscripts \
      distro-info \
      fakeroot \
      libdpkg-perl \
      libpam-cgfs \
      lxc \
      lxcfs \
      lxc-templates \
      procps \
      python3 \
      python3-debian \
      rsync \
      sudo \
      uidmap \
      ${NULL+}
    # Thanks to the authors of https://salsa.debian.org/salsa-ci-team/pipeline/
    # for this magic
    - /etc/init.d/lxc-net start
    - /etc/init.d/lxc start
    - umount -R /sys/fs/cgroup
    - mount -t cgroup2 -o rw,nosuid,nodev,noexec,relatime cgroup2 /sys/fs/cgroup
    # Run as non-root to look for regressions in virt-lxc --sudo
    - adduser autopkgtest
    - "echo 'autopkgtest ALL=(ALL:ALL) NOPASSWD: ALL' > sudoers"
    - install -m440 sudoers /etc/sudoers.d/autopkgtest-lxc
    - AUTOPKGTEST_TEST_UNINSTALLED=yes runuser -u autopkgtest -- sh -x debian/tests/lxc

# TODO: Should test lxd on Gitlab-CI, but it needs non-trivial setup

# We intentionally don't run d/tests/podman-init here. It works in a local
# qemu VM, but doesn't work in Salsa-CI's Docker container due to problems
# with /sys/fs/cgroup (unlike d/tests/podman, which works in privileged
# Docker, even though it likely wouldn't work in an unprivileged container).
test-podman:
  stage: test
  needs: []
  image: registry.salsa.debian.org/salsa-ci-team/pipeline/autopkgtest
  script:
    - apt-get update
    - |
      apt-get install -y --no-install-recommends \
      adduser \
      apt-utils \
      autodep8 \
      build-essential \
      buildah \
      ca-certificates \
      catatonit \
      debhelper \
      debootstrap \
      devscripts \
      fakeroot \
      golang-github-containernetworking-plugin-dnsname \
      libdpkg-perl \
      podman \
      python3 \
      python3-debian \
      python3-distro-info \
      slirp4netns \
      uidmap \
      ${NULL+}
    - adduser autopkgtest
    - AUTOPKGTEST_TEST_UNINSTALLED=yes runuser -u autopkgtest -- sh -x debian/tests/podman

test-schroot:
  stage: test
  needs: []
  image: debian:stable
  script:
    - apt-get update
    - |
      apt-get install -y --no-install-recommends \
      apt-utils \
      autodep8 \
      build-essential \
      debhelper \
      debootstrap \
      devscripts \
      fakeroot \
      libdpkg-perl \
      procps \
      python3 \
      python3-debian \
      sbuild \
      schroot \
      ${NULL+}
    - mount -t tmpfs -o mode=0755,size=50% schrootunpack /var/lib/schroot/unpack
    - AUTOPKGTEST_TEST_UNINSTALLED=yes sh -x debian/tests/schroot

test-unshare:
  stage: test
  needs: []
  image: debian:stable
  script:
    - apt-get update
    - |
      apt-get install -y \
      apt-utils \
      autodep8 \
      build-essential \
      debhelper \
      devscripts \
      libdpkg-perl \
      procps \
      python3 \
      python3-debian \
      mmdebstrap \
      uidmap \
      ${NULL+}
    - adduser autopkgtest
    - AUTOPKGTEST_TEST_UNINSTALLED=yes runuser -u autopkgtest -- sh -x debian/tests/unshare

# Include and configure the Debian pipeline maintained by the Salsa CI team

include:
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/recipes/debian.yml

variables:
  # Stricter lintian
  SALSA_CI_LINTIAN_FAIL_WARNING: 1
  SALSA_CI_LINTIAN_SHOW_OVERRIDES: 1
  # We already run tests in separate jobs, let's skip extra runs.
  SALSA_CI_DPKG_BUILDPACKAGE_ARGS: --build-profiles=nocheck
  SALSA_CI_DISABLE_AUTOPKGTEST: 1
  # Disable blhc: we don't compile anything
  SALSA_CI_DISABLE_BLHC: 1
  # autopkgtest is an arch: all Python package, so these tests don't add much.
  # Note that a full build (dpkg-buildpackage --build=full) is always performed
  # at the pipeline 'build' stage.
  SALSA_CI_DISABLE_BUILD_PACKAGE_ALL: 1
  SALSA_CI_DISABLE_BUILD_PACKAGE_ANY: 1
  SALSA_CI_DISABLE_BUILD_PACKAGE_I386: 1
  SALSA_CI_DISABLE_CROSSBUILD_ARM64: 1

# Adapted from https://salsa.debian.org/pkg-debconf/debconf/-/blob/master/debian/salsa-ci.yml
pre-commit:
  stage: .pre
  image: debian:testing
  variables:
    XDG_CACHE_HOME: "$CI_PROJECT_DIR/.cache"
  before_script:
    - apt-get --yes --update -q install git pre-commit
  script:
    - pre-commit run --all-files --show-diff-on-failure
  cache:
    key: $CI_JOB_NAME
    paths:
      - .cache/pre-commit
